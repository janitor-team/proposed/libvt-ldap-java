/*
  $Id: LdapConnStrategyTest.java 1442 2010-07-01 18:05:58Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1442 $
  Updated: $Date: 2010-07-01 20:05:58 +0200 (Thu, 01 Jul 2010) $
*/
package edu.vt.middleware.ldap;

import edu.vt.middleware.ldap.handler.ConnectionHandler;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

/**
 * Unit test for {@link ConnectionHandler} with different strategies.
 *
 * @author  Middleware Services
 * @version  $Revision: 1442 $
 */
public class LdapConnStrategyTest
{


  /** @throws  Exception  On test failure. */
  @Test(groups = {"ldaptest"})
  public void connect()
    throws Exception
  {
    final Ldap l = new Ldap();
    l.loadFromProperties(
      LdapConnStrategyTest.class.getResourceAsStream("/ldap.conn.properties"));

    AssertJUnit.assertTrue(l.connect());
    l.close();
    AssertJUnit.assertTrue(l.connect());
    l.close();
    AssertJUnit.assertTrue(l.connect());

    l.getLdapConfig().getConnectionHandler().setConnectionStrategy(
      ConnectionHandler.ConnectionStrategy.DEFAULT);
    AssertJUnit.assertTrue(l.connect());
    l.close();
    AssertJUnit.assertTrue(l.connect());
    l.close();
    AssertJUnit.assertTrue(l.connect());

    l.getLdapConfig().getConnectionHandler().setConnectionStrategy(
      ConnectionHandler.ConnectionStrategy.ACTIVE_PASSIVE);
    AssertJUnit.assertTrue(l.connect());
    l.close();
    AssertJUnit.assertTrue(l.connect());
    l.close();
    AssertJUnit.assertTrue(l.connect());

    l.getLdapConfig().getConnectionHandler().setConnectionStrategy(
      ConnectionHandler.ConnectionStrategy.RANDOM);
    AssertJUnit.assertTrue(l.connect());
    l.close();
    AssertJUnit.assertTrue(l.connect());
    l.close();
    AssertJUnit.assertTrue(l.connect());
    l.close();
  }
}
