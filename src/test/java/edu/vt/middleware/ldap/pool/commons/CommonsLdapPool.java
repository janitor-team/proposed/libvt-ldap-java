/*
  $Id: CommonsLdapPool.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.pool.commons;

import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

/**
 * <code>CommonsLdapPool</code> provides a implementation of a commons pooling
 * <code>GenericObjectPool</code> for testing.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $ $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
 */
public class CommonsLdapPool extends GenericObjectPool
{


  /** Creates a new ldap pool using {@link DefaultLdapPoolableObjectFactory}. */
  public CommonsLdapPool()
  {
    this(new DefaultLdapPoolableObjectFactory());
  }


  /**
   * Creates a new ldap pool using the supplied poolable object factory.
   *
   * @param  poolableObjectFactory  to create Ldap objects with
   */
  public CommonsLdapPool(final PoolableObjectFactory poolableObjectFactory)
  {
    super(poolableObjectFactory);
  }
}
