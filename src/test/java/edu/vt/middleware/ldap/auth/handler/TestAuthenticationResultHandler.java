/*
  $Id: TestAuthenticationResultHandler.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.auth.handler;

import java.util.HashMap;
import java.util.Map;

/**
 * <code>TestAuthenticationResultHandler</code>.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $ $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
 */
public class TestAuthenticationResultHandler
  implements AuthenticationResultHandler
{

  /** results. */
  private Map<String, Boolean> results = new HashMap<String, Boolean>();


  /** {@inheritDoc} */
  public void process(final AuthenticationCriteria ac, final boolean success)
  {
    this.results.put(ac.getDn(), Boolean.valueOf(success));
  }


  /**
   * Returns the authentication results.
   *
   * @return  authentication results
   */
  public Map<String, Boolean> getResults()
  {
    return this.results;
  }
}
