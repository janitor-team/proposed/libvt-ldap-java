/*
  $Id: LdapBeanProvider.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.bean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <code>LdapBeanProvider</code> provides a single source for ldap bean types
 * and configuration.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $ $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
 */
public final class LdapBeanProvider
{

  /** bean factory class name. */
  public static final String BEAN_FACTORY =
    "edu.vt.middleware.ldap.beanFactory";

  /** Log for this class. */
  private static final Log LOG = LogFactory.getLog(LdapBeanProvider.class);

  /** single instance of the ldap bean provider. */
  private static final LdapBeanProvider INSTANCE = new LdapBeanProvider();

  /** factory used to create ldap beans. */
  private static LdapBeanFactory beanFactory;


  /** Default constructor. */
  private LdapBeanProvider()
  {
    final String beanFactoryClass = System.getProperty(BEAN_FACTORY);
    if (beanFactoryClass != null) {
      try {
        beanFactory = (LdapBeanFactory) Class.forName(beanFactoryClass)
            .newInstance();
        if (LOG.isInfoEnabled()) {
          LOG.info("Set provider bean factory to " + beanFactoryClass);
        }
      } catch (ClassNotFoundException e) {
        if (LOG.isErrorEnabled()) {
          LOG.error("Error instantiating " + beanFactoryClass, e);
        }
      } catch (InstantiationException e) {
        if (LOG.isErrorEnabled()) {
          LOG.error("Error instantiating " + beanFactoryClass, e);
        }
      } catch (IllegalAccessException e) {
        if (LOG.isErrorEnabled()) {
          LOG.error("Error instantiating " + beanFactoryClass, e);
        }
      }
    } else {
      // set default ldap bean factory to unordered
      beanFactory = new UnorderedLdapBeanFactory();
    }
  }


  /**
   * Returns the instance of this <code>LdapBeanProvider</code>.
   *
   * @return  <code>LdapBeanProvider</code>
   */
  public static LdapBeanProvider getInstance()
  {
    return INSTANCE;
  }


  /**
   * Returns the factory for creating ldap beans.
   *
   * @return  <code>LdapBeanFactory</code>
   */
  public static LdapBeanFactory getLdapBeanFactory()
  {
    return beanFactory;
  }


  /**
   * Sets the factory for creating ldap beans.
   *
   * @param  lbf  <code>LdapBeanFactory</code>
   */
  public static void setLdapBeanFactory(final LdapBeanFactory lbf)
  {
    if (lbf != null) {
      beanFactory = lbf;
    }
  }
}
