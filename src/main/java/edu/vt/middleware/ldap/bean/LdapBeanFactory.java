/*
  $Id: LdapBeanFactory.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.bean;

/**
 * <code>LdapBeanFactory</code> provides an interface for ldap bean type
 * factories.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $ $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
 */
public interface LdapBeanFactory
{


  /**
   * Create a new instance of <code>LdapResult</code>.
   *
   * @return  <code>LdapResult</code>
   */
  LdapResult newLdapResult();


  /**
   * Create a new instance of <code>LdapEntry</code>.
   *
   * @return  <code>LdapEntry</code>
   */
  LdapEntry newLdapEntry();


  /**
   * Create a new instance of <code>LdapAttributes</code>.
   *
   * @return  <code>LdapAttributes</code>
   */
  LdapAttributes newLdapAttributes();


  /**
   * Create a new instance of <code>LdapAttribute</code>.
   *
   * @return  <code>LdapAttribute</code>
   */
  LdapAttribute newLdapAttribute();
}
