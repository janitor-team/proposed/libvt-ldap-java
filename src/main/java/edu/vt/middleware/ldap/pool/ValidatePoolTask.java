/*
  $Id: ValidatePoolTask.java 2790 2013-07-11 17:50:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 2790 $
  Updated: $Date: 2013-07-11 19:50:53 +0200 (Thu, 11 Jul 2013) $
*/
package edu.vt.middleware.ldap.pool;

import java.util.TimerTask;
import edu.vt.middleware.ldap.BaseLdap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <code>ValidatePoolTask</code> is a periodic task that checks that every ldap
 * object in the pool is valid. Objects that don't pass validation are removed.
 *
 * @param  <T>  type of ldap object
 *
 * @author  Middleware Services
 * @version  $Revision: 2790 $ $Date: 2013-07-11 19:50:53 +0200 (Thu, 11 Jul 2013) $
 */
public class ValidatePoolTask<T extends BaseLdap> extends TimerTask
{

  /** Log for this class. */
  protected final Log logger = LogFactory.getLog(this.getClass());

  /** Pool to clean. */
  private LdapPool<T> pool;


  /**
   * Creates a new task to periodically validate the supplied pool.
   *
   * @param  lp  ldap pool to periodically validate
   */
  public ValidatePoolTask(final LdapPool<T> lp)
  {
    this.pool = lp;
  }


  /**
   * This attempts to validate idle objects in a pool. See {@link
   * LdapPool#validate()}.
   */
  public void run()
  {
    if (this.logger.isDebugEnabled()) {
      this.logger.debug("Begin validate task for " + this.pool);
    }
    try {
      this.pool.validate();
    } catch (Exception e) {
      if (this.logger.isErrorEnabled()) {
        this.logger.error("Validate task failed for " + this.pool, e); 
      }
    }
    if (this.logger.isDebugEnabled()) {
      this.logger.debug("End validate task for " + this.pool);
    }
  }
}
