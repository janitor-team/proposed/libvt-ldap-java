/*
  $Id: LdapPassivator.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.pool;

import edu.vt.middleware.ldap.BaseLdap;

/**
 * <code>LdapPasivator</code> provides an interface for passivating ldap objects
 * when they are checked back into the pool.
 *
 * @param  <T>  type of ldap object
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $ $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
 */
public interface LdapPassivator<T extends BaseLdap>
{


  /**
   * Passivate the supplied ldap object.
   *
   * @param  t  ldap object
   *
   * @return  whether passivation was successful
   */
  boolean passivate(T t);
}
