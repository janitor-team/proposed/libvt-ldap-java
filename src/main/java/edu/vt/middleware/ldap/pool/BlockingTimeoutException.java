/*
  $Id: BlockingTimeoutException.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.pool;

/**
 * <code>BlockingTimeoutException</code> is thrown when a blocking operation
 * times out. See {@link BlockingLdapPool#checkOut()}.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $
 */
public class BlockingTimeoutException extends LdapPoolException
{

  /** serialVersionUID. */
  private static final long serialVersionUID = -5152940431346111294L;


  /**
   * This creates a new <code>BlockingTimeoutException</code> with the supplied
   * <code>String</code>.
   *
   * @param  msg  <code>String</code>
   */
  public BlockingTimeoutException(final String msg)
  {
    super(msg);
  }


  /**
   * This creates a new <code>BlockingTimeoutException</code> with the supplied
   * <code>Exception</code>.
   *
   * @param  e  <code>Exception</code>
   */
  public BlockingTimeoutException(final Exception e)
  {
    super(e);
  }


  /**
   * This creates a new <code>BlockingTimeoutException</code> with the supplied
   * <code>String</code> and <code>Exception</code>.
   *
   * @param  msg  <code>String</code>
   * @param  e  <code>Exception</code>
   */
  public BlockingTimeoutException(final String msg, final Exception e)
  {
    super(msg, e);
  }
}
