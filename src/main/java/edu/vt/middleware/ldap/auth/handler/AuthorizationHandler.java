/*
  $Id: AuthorizationHandler.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.auth.handler;

import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;

/**
 * AuthorizationHandler provides processing of authorization queries after
 * authentication has succeeded.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $
 */
public interface AuthorizationHandler
{


  /**
   * Process an authorization after an ldap authentication. The supplied
   * LdapContext should <b>not</b> be closed in this method. Implementations
   * should throw <code>AuthorizationException</code> to indicate an
   * authorization failure.
   *
   * @param  ac  <code>AuthenticationCriteria</code> used to perform the
   * authentication
   * @param  ctx  <code>LdapContext</code> authenticated context used to perform
   * the bind
   *
   * @throws  AuthorizationException  if authorization fails
   * @throws  NamingException  if an LDAP error occurs
   */
  void process(AuthenticationCriteria ac, LdapContext ctx)
    throws NamingException;
}
