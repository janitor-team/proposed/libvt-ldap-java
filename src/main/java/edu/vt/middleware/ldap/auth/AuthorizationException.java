/*
  $Id: AuthorizationException.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.auth;

import javax.naming.NamingException;

/**
 * <code>AuthorizationException</code> is thrown when an attempt to authorize a
 * user fails.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $
 */
public class AuthorizationException extends NamingException
{

  /** serialVersionUID. */
  private static final long serialVersionUID = -6290236661997869406L;


  /** Default constructor. */
  public AuthorizationException()
  {
    super();
  }


  /**
   * This creates a new <code>AuthorizationException</code> with the supplied
   * <code>String</code>.
   *
   * @param  msg  <code>String</code>
   */
  public AuthorizationException(final String msg)
  {
    super(msg);
  }
}
