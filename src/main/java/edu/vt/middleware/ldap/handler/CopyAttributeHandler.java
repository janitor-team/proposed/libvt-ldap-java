/*
  $Id: CopyAttributeHandler.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.handler;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.BasicAttribute;

/**
 * <code>CopyAttributeHandler</code> converts a NamingEnumeration of attribute
 * into a List of attribute.
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $ $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
 */
public class CopyAttributeHandler extends CopyResultHandler<Attribute>
  implements AttributeHandler
{


  /**
   * This will return a deep copy of the supplied <code>Attribute</code>.
   *
   * @param  sc  <code>SearchCriteria</code> used to find enumeration
   * @param  attr  <code>Attribute</code> to copy
   *
   * @return  <code>Attribute</code>
   *
   * @throws  NamingException  if the attribute values cannot be read
   */
  protected Attribute processResult(
    final SearchCriteria sc,
    final Attribute attr)
    throws NamingException
  {
    Attribute newAttr = null;
    if (attr != null) {
      newAttr = new BasicAttribute(attr.getID(), attr.isOrdered());

      final NamingEnumeration<?> en = attr.getAll();
      while (en.hasMore()) {
        newAttr.add(this.processValue(sc, en.next()));
      }
    }
    return newAttr;
  }


  /**
   * This returns the supplied value unaltered.
   *
   * @param  sc  <code>LdapSearchCritieria</code> used to find enumeration
   * @param  value  <code>Object</code> to process
   *
   * @return  <code>Object</code>
   */
  protected Object processValue(final SearchCriteria sc, final Object value)
  {
    return value;
  }
}
