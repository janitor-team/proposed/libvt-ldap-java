/*
  $Id: ResultHandler.java 1330 2010-05-23 22:10:53Z dfisher $

  Copyright (C) 2003-2010 Virginia Tech.
  All rights reserved.

  SEE LICENSE FOR MORE INFORMATION

  Author:  Middleware Services
  Email:   middleware@vt.edu
  Version: $Revision: 1330 $
  Updated: $Date: 2010-05-24 00:10:53 +0200 (Mon, 24 May 2010) $
*/
package edu.vt.middleware.ldap.handler;

import java.util.List;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

/**
 * ResultHandler provides post search processing of ldap results.
 *
 * @param  <R>  type of result
 * @param  <O>  type of output
 *
 * @author  Middleware Services
 * @version  $Revision: 1330 $
 */
public interface ResultHandler<R, O>
{


  /**
   * Process the results from an ldap search.
   *
   * @param  sc  <code>SearchCriteria</code> used to perform the search
   * @param  en  <code>NamingEnumeration</code> of search results
   *
   * @return  <code>List</code> of result objects
   *
   * @throws  NamingException  if the LDAP returns an error
   */
  List<O> process(SearchCriteria sc, NamingEnumeration<? extends R> en)
    throws NamingException;


  /**
   * Process the results from an ldap search.
   *
   * @param  sc  <code>SearchCriteria</code> used to perform the search
   * @param  en  <code>NamingEnumeration</code> of search results
   * @param  ignore  <code>Class[]</code> of exception types to ignore results
   *
   * @return  <code>List</code> of result objects
   *
   * @throws  NamingException  if the LDAP returns an error
   */
  List<O> process(
    SearchCriteria sc,
    NamingEnumeration<? extends R> en,
    Class<?>[] ignore)
    throws NamingException;


  /**
   * Process the results from an ldap search.
   *
   * @param  sc  <code>SearchCriteria</code> used to perform the search
   * @param  l  <code>List</code> of search results
   *
   * @return  <code>List</code> of result objects
   *
   * @throws  NamingException  if the LDAP returns an error
   */
  List<O> process(SearchCriteria sc, List<? extends R> l)
    throws NamingException;
}
